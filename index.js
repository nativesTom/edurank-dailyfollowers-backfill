const moment = require('moment')
// const MONGODB_URI = "mongodb://erProd:qMR3fKLjBLhQ65uwEAUJcRvR@edurank-prod-shard-00-00-61ojv.mongodb.net:27017,edurank-prod-shard-00-01-61ojv.mongodb.net:27017,edurank-prod-shard-00-02-61ojv.mongodb.net:27017/er-production?ssl=true&replicaSet=edurank-prod-shard-0&authSource=admin&retryWrites=true&readPreference=secondaryPreferred"
const MONGODB_URI = "mongodb://mahesh:Ci8pLsbuC18EcAtm3Rl2XRpQvzqwHxwh@edurank-dev-shard-00-00.61ojv.mongodb.net:27017,edurank-dev-shard-00-01.61ojv.mongodb.net:27017,edurank-dev-shard-00-02.61ojv.mongodb.net:27017/er-production?ssl=true&replicaSet=edurank-dev-shard-0&authSource=admin&retryWrites=true&w=majority"
const MongoClient = require('mongodb').MongoClient;
ObjectID = require('mongodb').ObjectID

const varianceConstant = 1.1  //decrease this for less variance
var missingDays;
var platformArray = ['instagram', 'facebook', 'twitter', 'youtube']

var count = 0
var dailyFollowers
let institutions

init()

//functions------------------------------------------------------------------------------------

function init() {
    connectToClient()
    .then(generateFollowers)
    .then(updateDB)
    .catch(err => console.log(err))
}

function connectToClient() {
    return new Promise(async (resolve, reject) => {
        try {
            var client = await MongoClient.connect(MONGODB_URI, { useNewUrlParser: true })
            if (!client) {
                reject('client not available')
            }
            const db = client.db("er-production");
            institutions = db.collection('institutions');   
            dailyFollowers = db.collection('dailyFollowers');
            resolve(client)
        } catch (err) {
            reject('error connection to client ' + err)
        }      
    })
}

function generateFollowers(client) {
    return new Promise(async (resolve, reject) => {
        try {

            var update = []
        
            // go out and get institutions
            var institutionsDocs = await institutions.find({}, {_id: true, name: true}).toArray()
            
            // loop over institutions then platforms then dates
            for (const institution of institutionsDocs) {
                count++ 
                console.log(count + ' / ' + institutionsDocs.length)   
                for (const platform of platformArray) {
                    var monthArray = [
                        moment(),
                        moment().subtract(1, 'Months'),
                        moment().subtract(2, 'Months')
                    ]
                    for (const date of monthArray) {

                        var month = date.month()
                        var year = date.year()
        
                        var docs = await dailyFollowers.find({_institutionId: institution._id, month: month, year: year, platform: platform}, {day: true, followers: true}).toArray()    
        
                        if (docs.length === 0) continue
        
                        missingDays = getMissingDays(docs, date)
        
                        for (const day of missingDays) {
                            
                            // clone targetDate so nextDay can be calculated without it changing original
                            var targetDate = moment(year + "-" + (month + 1) + "-" + day + " 12:00")
                            var duplicateTargetDate = targetDate.clone()
                            var nextDay = duplicateTargetDate.add(1, 'days')  

                            var followers = getFollowers(day, docs, targetDate)   // most stuff happens in here

                            var dailyFollowersDoc = {
                                _institutionId: new ObjectID(institution._id),
                                year: year,
                                month,
                                day,
                                platform,
                                followers,
                                collectionDate: new Date(nextDay)
                            }
        
                            // console.log(dailyFollowersDoc)   
                            update.push(dailyFollowersDoc) 
            
                        }  
                    }
                }
            }  
            
            // done iterating
            resolve(update) 

        } catch (err) {
            console.log('error generating followers')
            reject(err)
        }
    })
}

function updateDB(update) {
    return new Promise((resolve, reject) => {  
        console.log('updatingDB')         
        dailyFollowers.insertMany(update)
        .then((result) => {
            console.log(result)
            resolve()
        })
        .catch(err => { 
            console.log('error inserting') 
            reject(err)
        })
    })
}

function getFollowers(day, docs, targetDate) {
        
    var followers

    var prevPopulatedDay = getPreviousPopulatedDay(day)
    var nextPopulatedDay = getNextPopulatedDay(day, targetDate)
    var prevFollowers = getFollowersFromDay(prevPopulatedDay, docs)
    var nextFollowers = getFollowersFromDay(nextPopulatedDay, docs)

    var gapLength = nextPopulatedDay - prevPopulatedDay
    var followerDifference = nextFollowers - prevFollowers
    var daysSinceFirstDay = day - prevPopulatedDay

    // do some graduated data filling
    if (prevPopulatedDay && nextPopulatedDay) {
        var rigidFollowerData = ((followerDifference / gapLength) * daysSinceFirstDay) + prevFollowers
        followers = generateVariance(rigidFollowerData)
    }
    // if no previous data extend next data backwards
    if (!prevPopulatedDay && nextPopulatedDay) {
        followers = generateVariance(nextFollowers)
    }
    // if no following data extend previous data forwards
    if (prevPopulatedDay && !nextPopulatedDay) {
        followers = generateVariance(prevFollowers)
    }

    return Math.floor(followers) 
}

function generateVariance(data) {
    var min = Math.ceil(0.85)
    var max = Math.floor(0.999)
    var coefficient = Math.floor(Math.random() * (max - min + 1)) + min;
    data * (coefficient * varianceConstant)
    return data * coefficient
}

function getFollowersFromDay(day, docs) {
    for (var i = 0; i < docs.length; i++) {
        if (docs[i].day === day) return docs[i].followers
    }
}

// get days of month missing, iterate until either end of month or yesterday if this month, if day missing from doc then return
function getMissingDays(docs, date) {
    var missing = []
    var finish = date.month() === moment().month() ? date.date() - 1 : date.daysInMonth()
    for (var i = 1; i <= finish; i++) {
        var found = false
        // for each day create a moment object for tomorrow
        var tomorrow = moment(date.year() + "-" + (date.month() + 1) + "-" + i).add(1, 'days')
        for (var j = 0; j < docs.length; j++) {
            // compare tomorrow to the collection date
            if (tomorrow.isSame(moment(docs[j].collectionDate), 'day')) {
                found = true
                break
            }  
        }
        if (!found) missing.push(i)
    }
    return missing
}

// returns undefined if no precedent data is detected
function getPreviousPopulatedDay(day) {
    for (var i = day; i > 0; i--) {
        if (missingDays.indexOf(i) === -1) {
            return i
        }
    }
}

// returns undefined if no antecedent data is detected
function getNextPopulatedDay(day, targetDate) {
    var finish = targetDate.month() === moment().month() ? moment().date() -1 : targetDate.daysInMonth()
    for (var i = day; i <= finish; i++) {
        if (missingDays.indexOf(i) === -1) {
            return i
        }
    }
}













    









